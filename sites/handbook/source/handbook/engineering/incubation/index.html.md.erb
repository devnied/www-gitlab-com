---
layout: handbook-page-toc
title: Incubation Engineering Department
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Incubation Engineering Department

The Incubation Engineering Department within the Engineering Division focuses on projects that are pre-Product/Market fit. The projects they work on align with the term "new markets" from one of our [product investment types](/handbook/product/investment/#investment-types). They are ideas that may contribute to our revenue in 3-5 years time. Their focus should be to move fast, ship, get feedback, and [iterate](/handbook/values/#iteration). But first they've got to get from 0 to 1 and get something shipped.

We utilize [Single-engineer Groups](/company/team/structure/#single-engineer-groups) to draw benefits, but not distractions, from within the larger company and [GitLab project](https://gitlab.com/gitlab-org/gitlab) to maintain that focus. The Single-engineer group encompasses all of product development (product management, engineering, design, and quality) at the smallest scale. They are free to learn from, and collaborate with, those larger departments at GitLab but not at the expense of slowing down unnecessarily.

The Department Head is the [VP of Incubation Engineering](/job-families/engineering/vp-of-incubation-engineering/).

### Investment Framework

The aim of the SEG's in the Incubation Engineering Department is to get ideas for new markets into the Viable category from our [maturity](https://about.gitlab.com/direction/maturity/) framework.  Ideas that we successfully incubate will become a stage or feature within the relevant areas of our product, with full Product, Engineering, UX and Infrastructure support for future development.

In order to determine our investment into an SEG, we use the following assumptions:

* SEGs have a 25% success rate (as opposed to 5% of VC funded companies), due to the existing market reach and technical foundations of the GitLab product and the culture, processes and support from the wider organisation.
* SEG's mature categories at a 50% faster rate than regular GitLab Category Maturity features, due to the narrow focus of our investments.
* SEG's ramp up revenue as good as the best startups in the world (5 year to $100M ARR)

Reference:

* <https://kimchihill.com/2020/04/09/path-to-100m-arr/>
* <https://www.stephnass.com/blog/saas-ipo-roadmap>

### FY22 Direction

The Incubation Engineering Department is new, and the focus for FY22 is to:

* Hire team members that work as Single-Engineer Groups to deliver the departments priorities.
* Develop a repeatable process and appropriate performance indicators that allow the team to measure and demonstrate their progress and impact for each of the areas we are interested in delivering.
* Develop a scoring framework and methodology to accept and prioritise new ideas.

The initial iteration towards this Direction is covered in the [FY22-Q2 Incubation Engineering Department OKRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11187).

## Current Focus

### [ModelOps](/handbook/engineering/incubation/mlops)

The ModelOps Group will be focused on enabling data teams to build, test, and deploy their machine learning models. This will be net new functionality within GitLab and will bridge the gap between DataOps teams and ML/AI within production. ModelOps will provide tuning, testing, and deployment of machine learning models including version control and partial rollout and rollback.

### [Monitor APM](/handbook/engineering/incubation/monitor-apm)

The Monitor APM Group aims to integrate monitoring and observability into our DevOps Platform in order to provide a convenient and cost effective solution that allows our customers to monitor the state of their applications, understand how changes they make can impact their applications performance characteristics and give them the tools to resolve issues that arise.

This group is split into three APM's that will focus on each part of the architecture:

**Monitor APM:Agent**

Collecting logs and traces from production systems.

**Monitor APM:Storage**

Storing and querying APM data.

**Monitor APM:Visualisation**

Charts and tables of APM data, with UX for querying and drilling down.

### [5 min prod app](/handbook/engineering/incubation/5-min-production)

The 5 Minute Production App Group will look at our developer onboarding experience and how to make it easier for Web App developers to quickly get their application deployed to Production, and to have the re-assurance that it can scale when needed.

### [Real-time Collaboration](/handbook/engineering/incubation/real-time-collaboration)

The Real-time Collaboration Group is gathering feedback on what merge requests and code review in GitLab may be in the future and how we can significantly decrease the cycle time, increase the efficiency of code review, and create better ways of collaborating through real-time experiences.

### [Integrated Development Environment](/handbook/engineering/incubation/integrated-development-environment)

The Integrated Development Environment SEG will explore how we can increase customer usage and company revenue by delivering a cloud hosted development environment natively within GitLab.com.

### [Error Tracking](/handbook/engineering/incubation/error-tracking)

The goal of the Error Tracking SEG is to provide monitoring tools for that allow developers to identify and resolve issues in their production environments.

### [DevOps for Mobile Apps](/handbook/engineering/incubation/devops-for-mobile)

The goal of the DevOps for Mobile Apps SEG is to improve the experience for Developers targeting mobile platforms by providing targeted CI/CD capabilities and workflows that improve the experience of provisioning and deploying mobile apps.

### [Jamstack](/handbook/engineering/incubation/jamstack)

The Jamstack group aims to enable Front End developers to simplify their toolkit by using GitLab to manage, build, and deploy externally-facing, static websites using the Jamstack architecture.

### Backlog

We use the [RICE Framework](https://about.gitlab.com/handbook/product/product-processes/#using-the-rice-framework) for prioritization of ideas.  

<table id="objectives-table-bizops">
  <tr>
    <th class="text-center">
        <h5>Reach</h5>
    </th>
    <th class="text-center">
        <h5>Impact</h5>
    </th>
    <th class="text-center">
        <h5>Confidence</h5>
    </th>
  </tr>
  <tr>
    <td>How many of our users, prospects, or customers will benefit from this feature.</td>
    <td>Revenue, risk, or cost benefits to GitLab and our customers.</td>
    <td>How well we understand the customer problem and our proposed solution.</td>
  </tr>
  <tr>
    <td>
    <ul>
    <li>10.0 = Impacts the <strong>Vast</strong> majority (~80% or greater)</li>
    <li>6.0 = Impacts a <strong>Large</strong> percentage (~50% to ~80%)</li>
    <li>3.0 = <strong>Significant</strong> reach (~25% to ~50%)</li>
    <li>1.5 = <strong>Small</strong> reach (~5% to ~25%)</li>
    <li>0.5 = <strong>Minimal</strong> reach (Less than ~5%)</li>
    </ul>
    </td>
    <td>
    <ul>
    <li>3 = <strong>Massive</strong></li>
    <li>2 = <strong>High</strong></li>
    <li>1 = <strong>Medium</strong></li>
    <li>0.5 = <strong>Low</strong></li>
    <li>0.25 = <strong>Minimal</strong></li>
    </ul>
    </td>
    <td>
    <ul>
    <li>2 = <strong>High</strong></li>
    <li>1 = <strong>Medium</strong></li>
    <li>0.5 = <strong>Low</strong></li>
    </ul>
    </td>
  </tr>
  <tr>
  <td colspan="3" class="text-center"><h5>Effort</h5></td>
  </tr>
  <tr>
  <td colspan="3">How many months before we can launch an MVP to gather feedback (1 - 3).</td>
  </tr>
  </table>

The total score is calculated by **(Reach x Impact x Confidence) / Effort**

The following table is driven by the relevant RICE labels on the issues themselves.  

If you would like to propose a Single Engineer Group please reach to our [VP of Incubation Engineering](https://gitlab.com/bmarnane).

<%= partial("direction/seg-table", :locals => { :label => "SingleEngineerGroups" }) %>


## Management Issue Board

[Incubation Engineering Issues](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/980804?scope=all&utf8=✓&label_name[]=Engineering%20Management&label_name[]=Incubation%20Engineering%20Department)
